import pylab as pl
import utils
import numpy as np
import qam16_tx, channel, utils, plots

class BerChecker(object):
    def __init__(self):
        pass
    
    @staticmethod
    def calc_rotation( rx,tx):
        rots = [np.pi/2,np.pi,np.pi*3/2]
        min_mse = np.mean(abs(rx-tx)**2)
        min_rot=0
        for rot in rots:
            mse = np.mean(abs(rx-tx*np.exp(1j*rot))**2)
            if mse<min_mse:
                min_mse=mse
                min_rot = rot
        return min_rot

    @staticmethod
    def align(rx,tx):
        Lsim=len(rx); # Assume tx has the same len, and it is even
        if Lsim%2 ==1:
            Lsim=Lsim-1
        tx=tx[0:Lsim]
        rx_diff = rx[0:Lsim-1]*np.conj(rx[1:Lsim])
        tx_diff = tx[0:Lsim-1]*np.conj(tx[1:Lsim])
        xcorr = np.correlate(rx_diff, tx_diff, 'same')
        imax = int(np.argmax(xcorr)-Lsim/2+1)

        rx_align = rx[imax:Lsim]
        tx_align = tx[0:Lsim-imax]

        # print(len(rx_align))
        # print(len(tx_align))

        # print(rx_align[0:5])
        # print(tx_align[0:5])

        return rx_align, tx_align
    
    def execute(self,rx,tx):
        rx_align, tx_align = self.align(rx,tx)
        rx_phase = self.calc_rotation(rx_align[0:100],tx_align[0:100])
        rx_align = rx_align*np.exp(-1j*rx_phase)
        rx_det = utils.slicer_qam16(rx_align)
                
        errors = (sum(rx_det != tx_align))
        nsymbs = len(rx_det);
        ser = float(errors)/nsymbs
        ber = ser/16.
        mse = np.mean(abs(rx_align-tx_align)**2 / abs(tx_align)**2 )

        return mse, ser, ber
        
if __name__=='__main__':
    tx=qam16_tx.Tx16QAM()
    ch = channel.Channel(17,0e6,32e9)
    
    tx_symbs = tx.execute(int(10e3-1))
    ch_out = ch.execute(tx_symbs)

    L=len(ch_out)
    
    y=np.zeros((L,), dtype=complex)
    y[10:L]=ch_out[0:L-10]

    bcheck = BerChecker()
    mse, ser, ber = bcheck.execute(y,tx_symbs)
    print (10*np.log10(mse))
    print(ser)
    print(ber)
    
        
