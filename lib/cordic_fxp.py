import math
import numpy as np
import pylab as pl

def flp2fxp(data, nbf, rounding=True):
    if rounding:
        data_fxp = int(round(data * 2**nbf))
    else:
        data_fxp = int(data * 2**nbf)
    return data_fxp


def fxp2flp(data, nbf):
    data_flp = data * 2**-nbf
    return data_flp
    
#CORDIC FXP, las entradas input_x e input_y se interpretan como enteros
def cordic_vectoring_fxp(input_x, input_y, iterations=14, nbf_phase=16, nbf_mod=8):

    #Defino caso corner para ser consistente con otras implementaciones
    if(input_x == 0 and input_y == 0):
        return 0,0
    
    #Cordic original funciona igual que arctg en el cuadrante 1 y 4
    # se extiende a los cuadrantes 2 y 3
    if(input_x < 0 and input_y<0):
        accum_x = -int(input_x)
        accum_y = -int(input_y)        
        accum_ang = flp2fxp(-1, nbf_phase)
    elif(input_x < 0 and input_y>=0):
        accum_x = -int(input_x)
        accum_y = -int(input_y)        
        accum_ang = flp2fxp(1, nbf_phase)
    else:
        accum_x = int(input_x)
        accum_y = int(input_y)
        accum_ang = 0

    accum_x = accum_x << iterations
    accum_y = accum_y << iterations    

    #Arctg lut, guarda arctg(2^-i) para las N iteraciones
    # module_const, recupera el modulo original del vector luego de las
    # rotaciones
    atan_lut = []
    module_const = 1
    for kk in range(iterations):
        atan_lut.append(flp2fxp(math.atan((2**-kk))/(math.pi), nbf_phase))
        module_const = module_const * (1.0/math.sqrt(1+2**(-2*kk)))

    for it in range(iterations):
        # print("\n", accum_x)
        # print(accum_y)
        # print('angle: ', accum_ang)
        if(accum_y > 0):
            new_accum_x = accum_x + (accum_y >> it)
            new_accum_y = accum_y - (accum_x >> it)
            accum_ang = accum_ang + atan_lut[it]
        else:
            new_accum_x = accum_x - (accum_y >> it)
            new_accum_y = accum_y + (accum_x >> it)
            accum_ang = accum_ang - atan_lut[it]
        accum_x = new_accum_x
        accum_y = new_accum_y

    mod_fxp = flp2fxp(accum_x*module_const, nbf_mod)
    mod_fxp = mod_fxp >> iterations
    return mod_fxp, accum_ang


def cart2pol (input_x, input_y):
    if(input_x < 0 and input_y<0):
        delta = -math.pi
    elif(input_x < 0 and input_y>0):
        delta = math.pi
    else:
        delta = 0
    if(input_x == 0):
        angle = math.pi/2
    else:
        angle = delta + math.atan(input_y/input_x)
    mod = math.sqrt(input_x**2+input_y**2)
    return mod, angle


def cordic_array_complex_fxp(samples, iterations, nbf_phase, nbf_mod):
    N = len(samples)
    module = np.zeros(N)
    angle = np.zeros(N)
    for kk in range(N):
        mod_fxp, angle_fxp = cordic_vectoring_fxp(samples[kk].real, samples[kk].imag,
                                                     iterations, nbf_phase, nbf_mod)

        module[kk] = fxp2flp(mod_fxp,nbf_mod)
        angle[kk] = math.pi*fxp2flp(angle_fxp,nbf_phase)

    return module, angle


if __name__=='__main__':

    #-------------------------------------------------------
    # Analisis de error de cuantizacion
    #-------------------------------------------------------
    N = 1000
    NBF = 8
    MAX_AMP = -1
    q_error = np.zeros(N)
    for kk in range(N):
        flp_sample = MAX_AMP*np.random.rand(1)[0]
        q_sample = fxp2flp(flp2fxp(flp_sample, NBF, True), NBF)
        q_error[kk] = flp_sample - q_sample

    pl.title("Distribucion error de cuantizacion para NBF={}".format(NBF))
    pl.axvline(-0.5*2**-NBF, ls=':', color='r', label='-1/2 LSB')
    pl.axvline(0.5*2**-NBF, ls='--', color='r', label=' 1/2 LSB')    
    pl.hist(q_error, label="Distribucion de error")
    pl.grid()
    pl.legend()
    pl.show()

    #-------------------------------------------------------
    # Analisis de casos corner y 4 cuadrantes
    #-------------------------------------------------------
    iterations = 14
    nbf_phase = 16
    nbf_mod = 8    
    print("Caso 0.0")
    a = 0 + 0j
    print(np.angle(a))
    print (cart2pol(a.real,a.imag))
    mod_fxp, angle_fxp = cordic_vectoring_fxp(a.real, a.imag, iterations, nbf_phase, nbf_mod)
    print(fxp2flp(mod_fxp,nbf_mod), math.pi*fxp2flp(angle_fxp,nbf_phase))
    print("1 cuadrant")
    a = 64 + 64j
    print(np.angle(a))
    print (cart2pol(64,64))
    mod_fxp, angle_fxp = cordic_vectoring_fxp(a.real, a.imag, iterations, nbf_phase, nbf_mod)
    print(fxp2flp(mod_fxp,nbf_mod), math.pi*fxp2flp(angle_fxp,nbf_phase))
    print("2 cuadrant")
    a = -64 + 64j
    print(np.angle(a))
    print (cart2pol(-64,64))
    mod_fxp, angle_fxp = cordic_vectoring_fxp(a.real, a.imag, iterations, nbf_phase, nbf_mod)
    print(fxp2flp(mod_fxp,nbf_mod), math.pi*fxp2flp(angle_fxp,nbf_phase))
    print("3 cuadrant")
    a = -64 - 64j
    print(np.angle(a))
    print (cart2pol(-64,-64))
    mod_fxp, angle_fxp = cordic_vectoring_fxp(a.real, a.imag, iterations, nbf_phase, nbf_mod)
    print(fxp2flp(mod_fxp,nbf_mod), math.pi*fxp2flp(angle_fxp,nbf_phase))
    print("4 cuadrant")
    a = 64 - 64j
    print(np.angle(a))
    print (cart2pol(64,-64))


    #-------------------------------------------------------
    # Analisis de convergencia de angulo
    #-------------------------------------------------------
    a = 128 - 83j
    N = 16
    nbf_phase = 14
    module = np.zeros(N)
    angle = np.zeros(N)
    for kk in range(N):
        module_fxp, angle_fxp = cordic_vectoring_fxp(a.real, a.imag, kk, nbf_phase=nbf_phase)
        angle[kk] = math.pi*fxp2flp(angle_fxp,nbf_phase)
        
    pl.axhline(np.angle(a), label="Fase ideal", ls='--', color='r')
    pl.plot(angle, '.-', label="Fase en iteracion N")
    pl.xlabel("Iteracion")
    pl.ylabel("Fase [Rad]")
    pl.grid()
    pl.legend()
    pl.show()



    #-------------------------------------------------------
    # Analisis de performance vs numero de iteraciones
    #-------------------------------------------------------
    def random_test(L, MAX_AMP, CORDIC_ITERATIONS, nbf_phase=16, nbf_mod=16):
        samples = np.random.randint(-MAX_AMP, MAX_AMP, L) + 1j*np.random.randint(-MAX_AMP, MAX_AMP, L)
        cordic_mod, cordic_angle = cordic_array_complex_fxp(samples, CORDIC_ITERATIONS, nbf_phase, nbf_mod)
        ideal_mod   = np.abs(samples)
        ideal_angle = np.angle(samples)
        error_mod   = ideal_mod - cordic_mod
        error_angle = ideal_angle - cordic_angle
        #Calcular SNR y error maximo
        snr_mod = 10*np.log10(np.mean(ideal_mod**2)/np.mean(error_mod**2))
        snr_angle = 10*np.log10(np.mean(ideal_angle**2)/np.mean(error_angle**2))
        maxerr_mod = max(abs(error_mod))
        maxerr_angle = max(abs(error_angle))
        return snr_mod, snr_angle, maxerr_mod, maxerr_angle

    L = 10000
    MAX_AMP = 256
    error_angle_list = []
    error_mod_list   = []
    snr_angle_list   = []
    snr_mod_list     = []
    iterations = np.arange(7,15)
    for it in iterations:
        snr_mod, snr_angle, maxerr_mod, maxerr_angle = random_test(L, MAX_AMP, it)
        error_angle_list.append(maxerr_angle)
        error_mod_list.append(maxerr_mod)
        snr_angle_list.append(snr_angle)
        snr_mod_list.append(snr_mod)

    pl.title("Error CORDIC vs numero de iteraciones")
    pl.plot(iterations, error_angle_list, '.-', label="Error max fase")
    pl.plot(iterations, error_mod_list, '.-', label="Error max mod")
    pl.ylabel("Error")
    pl.xlabel("Numero de iteraciones")
    pl.grid()
    pl.legend()
    pl.figure()

    pl.title("SNR CORDIC vs numero de iteraciones")
    pl.plot(iterations, snr_angle_list, '.-', label="SNR fase")
    pl.plot(iterations, snr_mod_list, '.-', label="SNR mod")
    pl.ylabel("SNR [dB]")
    pl.xlabel("Numero de iteraciones")
    pl.grid()
    pl.legend()
    pl.show()

    #-------------------------------------------------------
    # Analisis de performance vs numero de bits fraccionales
    #-------------------------------------------------------
    L = 10000
    MAX_AMP = 256
    error_angle_list = []
    error_mod_list   = []
    snr_angle_list   = []
    snr_mod_list     = []
    iterations = 10
    nbf_sweep = range(5, 12)
    for nbf in nbf_sweep:
        snr_mod, snr_angle, maxerr_mod, maxerr_angle = random_test(L, MAX_AMP, iterations, nbf, nbf)
        error_angle_list.append(maxerr_angle)
        error_mod_list.append(maxerr_mod)
        snr_angle_list.append(snr_angle)
        snr_mod_list.append(snr_mod)

    pl.title("Error CORDIC vs numero de bits fraccionales")
    pl.plot(nbf_sweep, error_angle_list, '.-', label="Error max fase")
    pl.plot(nbf_sweep, error_mod_list, '.-', label="Error max mod")
    pl.ylabel("Error")
    pl.xlabel("Numero de bits fraccionales")
    pl.grid()
    pl.legend()
    pl.figure()

    pl.title("SNR CORDIC vs numero de bits fraccionales")
    pl.plot(nbf_sweep, snr_angle_list, '.-', label="SNR fase")
    pl.plot(nbf_sweep, snr_mod_list, '.-', label="SNR mod")
    pl.ylabel("SNR [dB]")
    pl.xlabel("Numero de bits fraccionales")
    pl.grid()
    pl.legend()
    pl.show()
    


    #-------------------------------------------------------
    # Analisis de distrubicion y limite de error
    #-------------------------------------------------------
    L = 10000
    MAX_AMP = 128
    CORDIC_ITERATIONS = 8
    samples = np.random.randint(-MAX_AMP, MAX_AMP, L) + 1j*np.random.randint(-MAX_AMP, MAX_AMP, L)
    cordic_mod, cordic_angle = cordic_array_complex_fxp(samples, CORDIC_ITERATIONS, nbf_phase=24, nbf_mod=24)
    ideal_mod   = np.abs(samples)
    ideal_angle = np.angle(samples)
    error_mod   = ideal_mod - cordic_mod
    error_angle = ideal_angle - cordic_angle
    pl.plot(error_angle)
    pl.show()
    
    pl.title("Distribucion error de fase")
    pl.axvline(-math.atan(2**-(CORDIC_ITERATIONS-1)), ls=':', color='r', label='-arctg(2^-(N-1))')
    pl.axvline(math.atan(2**-(CORDIC_ITERATIONS-1)), ls='--', color='r', label='arctg(2^-(N-1))')
    pl.hist(error_angle, label="Distribucion de error")
    pl.grid()
    pl.legend()
    pl.show()


