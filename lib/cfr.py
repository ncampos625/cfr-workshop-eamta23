import pylab as pl
import numpy as np
from  utils import slicer_qam16_1sample,slicer_qam16,slicer_qam16_polar_1sample
from qam16_tx import Tx16QAM
from  channel import Channel
import plots
from ber_checker import BerChecker
from cordic import cordic_vectoring


class CFR_Cartesian_Ideal_FLP_NoLatency(object):
    def __init__(self, kp, ki):
        self.kp=kp
        self.ki=ki

    def execute(self,x):
        Lsim=len(x)
        y = np.zeros((Lsim,), dtype=complex)
        phase_error= np.zeros((Lsim,))
        integrator= np.zeros((Lsim,))
        proportional_part= np.zeros((Lsim,))
        nco_output= np.zeros((Lsim,))
        nco_input = np.zeros((Lsim,))
        Lsim=len(x)
        for idx in range(1,Lsim):
            # Product between input and nco_output
            recovered_tone_sample = np.exp(-1j*nco_output[idx-1])
            #print(recovered_tone_sample)
            y[idx]= x[idx]*recovered_tone_sample

            # Calc error phase
            # Slicer
            y_det = slicer_qam16_1sample(y[idx])
            phase_error[idx] = np.angle(y[idx]*np.conj(y_det))

            # PI Filter
            if idx<int(Lsim/4):
                kp = self.kp*8
                ki = self.ki*8
            else:
                kp = self.kp
                ki = self.ki
            proportional_part[idx]=phase_error[idx]*kp
            integrator[idx] = integrator[idx-1] + ki*phase_error[idx]

            # NCO
            nco_input[idx] = proportional_part[idx]+integrator[idx]
            nco_output[idx] = np.mod ( nco_output[idx-1]+nco_input[idx], 2*np.pi)
            
        return y, phase_error,integrator,proportional_part,nco_input,nco_output 

class CFR_Polar_Ideal_FLP_NoLatency(object):
    def __init__(self, kp, ki):
        self.kp=kp
        self.ki=ki

    def execute(self,x):
        Lsim=len(x)
        y = np.zeros((Lsim,), dtype=complex)
        y_mod = np.zeros((Lsim,))
        y_phase = np.zeros((Lsim,) )
        x_phase = np.zeros((Lsim,) )
        phase_error= np.zeros((Lsim,))
        integrator= np.zeros((Lsim,))
        proportional_part= np.zeros((Lsim,))
        nco_output= np.zeros((Lsim,))
        nco_input = np.zeros((Lsim,))
        Lsim=len(x)
        for idx in range(1,Lsim):
            # Product between input and nco_output
            x_phase[idx] = np.angle(x[idx])
            x_mod = abs(x[idx])
            
            y_phase[idx]= x_phase[idx]-nco_output[idx-1]
            y_mod[idx] = x_mod

            # Calc error phase
            # Slicer
            y[idx] = y_mod[idx]*np.exp(1j*y_phase[idx])
            y_det = slicer_qam16_1sample(y[idx])
            phase_error[idx] = np.angle(y[idx]*np.conj(y_det))

            # PI Filter
            if idx<int(Lsim/4):
                kp = self.kp*8
                ki = self.ki*8
            else:
                kp = self.kp
                ki = self.ki
            proportional_part[idx]=phase_error[idx]*kp
            integrator[idx] = integrator[idx-1] + ki*phase_error[idx]

            # NCO
            nco_input[idx] = proportional_part[idx]+integrator[idx]
            nco_output[idx] = np.mod ( nco_output[idx-1]+nco_input[idx], 2*np.pi)

        return y, phase_error,integrator,proportional_part,nco_input,nco_output, x_phase, y_phase, y_mod

class CFR_Polar_SimpSlicer_FLP_NoLatency(object):
    def __init__(self, kp, ki):
        self.kp=kp
        self.ki=ki

    def execute(self,x):
        Lsim=len(x)
        y = np.zeros((Lsim,), dtype=complex)
        y_mod = np.zeros((Lsim,))
        y_phase = np.zeros((Lsim,) )
        x_phase = np.zeros((Lsim,) )
        phase_error= np.zeros((Lsim,))
        integrator= np.zeros((Lsim,))
        proportional_part= np.zeros((Lsim,))
        nco_output= np.zeros((Lsim,))
        nco_input = np.zeros((Lsim,))
        Lsim=len(x)
        for idx in range(1,Lsim):
            # Product between input and nco_output
            x_phase[idx] = np.angle(x[idx])
            x_mod = abs(x[idx])
            
            y_phase[idx]= x_phase[idx]-nco_output[idx-1]
            y_mod[idx] = x_mod
            y[idx] = y_mod[idx]*np.exp(1j*y_phase[idx])

            # Calc error phase
            # Slicer
            #y[idx] = y_mod[idx]*np.exp(1j*y_phase[idx])
            #y_det = slicer_qam16_1sample(y[idx])
            #phase_error[idx] = np.mod(y_phase[idx],np.pi/2) - np.mod(np.angle(y_det),np.pi/2)

            phase_error[idx] = np.mod(y_phase[idx],np.pi/2) - slicer_qam16_polar_1sample(y_mod[idx], y_phase[idx])

            # if idx<20:
            #     #print(np.mod(np.angle(y_det),np.pi/2)/np.pi)
            #     print("\t",slicer_qam16_polar_1sample(y_mod[idx], y_phase[idx])/np.pi)
            #     print(y[idx])
            #     print (y_mod[idx])
            #     print (np.mod(y_phase[idx],np.pi/2)/np.pi)
        
            # PI Filter
            if idx<int(Lsim/4):
                kp = self.kp*8
                ki = self.ki*8
            else:
                kp = self.kp
                ki = self.ki
            proportional_part[idx]=phase_error[idx]*kp
            integrator[idx] = integrator[idx-1] + ki*phase_error[idx]

            # NCO
            nco_input[idx] = proportional_part[idx]+integrator[idx]
            nco_output[idx] = np.mod ( nco_output[idx-1]+nco_input[idx], 2*np.pi)


        return y, phase_error,integrator,proportional_part,nco_input,nco_output, x_phase, y_phase, y_mod


class CFR_Polar_SimpSlicer_Cordic_FLP_NoLatency(object):
    def __init__(self, kp, ki, n_iterations):
        self.kp=kp
        self.ki=ki
        self.n_iterations = n_iterations

    def execute(self,x):
        Lsim=len(x)
        y = np.zeros((Lsim,), dtype=complex)
        y_mod = np.zeros((Lsim,))
        y_phase = np.zeros((Lsim,) )
        x_phase = np.zeros((Lsim,) )
        phase_error= np.zeros((Lsim,))
        integrator= np.zeros((Lsim,))
        proportional_part= np.zeros((Lsim,))
        nco_output= np.zeros((Lsim,))
        nco_input = np.zeros((Lsim,))
        Lsim=len(x)
        for idx in range(1,Lsim):
            # Product between input and nco_output
            x_mod, x_phase[idx] = cordic_vectoring(x[idx].real, x[idx].imag, self.n_iterations)
            
            y_phase[idx]= x_phase[idx]-nco_output[idx-1]
            y_mod[idx] = x_mod
            y[idx] = y_mod[idx]*np.exp(1j*y_phase[idx])

            # Calc error phase
            # Slicer
            #y[idx] = y_mod[idx]*np.exp(1j*y_phase[idx])
            #y_det = slicer_qam16_1sample(y[idx])
            #phase_error[idx] = np.mod(y_phase[idx],np.pi/2) - np.mod(np.angle(y_det),np.pi/2)

            phase_error[idx] = np.mod(y_phase[idx],np.pi/2) - slicer_qam16_polar_1sample(y_mod[idx], y_phase[idx])

            # if idx<20:
            #     #print(np.mod(np.angle(y_det),np.pi/2)/np.pi)
            #     print("\t",slicer_qam16_polar_1sample(y_mod[idx], y_phase[idx])/np.pi)
            #     print(y[idx])
            #     print (y_mod[idx])
            #     print (np.mod(y_phase[idx],np.pi/2)/np.pi)
        
            # PI Filter
            if idx<int(Lsim/4):
                kp = self.kp*8
                ki = self.ki*8
            else:
                kp = self.kp
                ki = self.ki
            proportional_part[idx]=phase_error[idx]*kp
            integrator[idx] = integrator[idx-1] + ki*phase_error[idx]

            # NCO
            nco_input[idx] = proportional_part[idx]+integrator[idx]
            nco_output[idx] = np.mod ( nco_output[idx-1]+nco_input[idx], 2*np.pi)


        return y, phase_error,integrator,proportional_part,nco_input,nco_output, x_phase, y_phase, y_mod

class CFR_Polar_SimpSlicer_Cordic_FXP_NoLatency(object):
    pass

class CFR_Polar_FLP_wLatency(object):
    def __init__(self, kp, ki, latency=5):
        self.kp=kp
        self.ki=ki
        self.latency = latency

    def execute(self,x):
        Lsim=len(x)
        y = np.zeros((Lsim,), dtype=complex)
        y_mod = np.zeros((Lsim,))
        y_phase = np.zeros((Lsim,) )
        x_phase = np.zeros((Lsim,) )
        phase_error= np.zeros((Lsim,))
        integrator= np.zeros((Lsim,))
        proportional_part= np.zeros((Lsim,))
        nco_output= np.zeros((Lsim,))
        nco_input = np.zeros((Lsim,))
        Lsim=len(x)
        for idx in range(1+self.latency,Lsim):
            # Product between input and nco_output
            x_phase[idx] = np.angle(x[idx])
            x_mod = abs(x[idx])
            
            y_phase[idx]= x_phase[idx]-nco_output[idx-1-self.latency]
            y_mod[idx] = x_mod

            # Calc error phase
            # Slicer
            y[idx] = y_mod[idx]*np.exp(1j*y_phase[idx])
            y_det = slicer_qam16_1sample(y[idx])
            phase_error[idx] = np.angle(y[idx]*np.conj(y_det))

            # PI Filter
            if idx<int(Lsim/4):
                kp = self.kp
                ki = self.ki
            else:
                kp = self.kp
                ki = self.ki
            proportional_part[idx]=phase_error[idx]*kp
            integrator[idx] = integrator[idx-1] + ki*phase_error[idx]

            # NCO
            nco_input[idx] = proportional_part[idx]+integrator[idx]
            nco_output[idx] = np.mod ( nco_output[idx-1]+nco_input[idx], 2*np.pi)

        return y, phase_error,integrator,proportional_part,nco_input,nco_output, x_phase, y_phase, y_mod



if __name__=='__main__':
    BR=32e9
    Lsim=100e3
    
    tx=Tx16QAM()
    cfr_cart = CFR_Cartesian_Ideal_FLP_NoLatency(1e-3,1e-3/1000)
    cfr_polar = CFR_Polar_SimpSlicer_FLP_NoLatency(1e-3,1e-3/1000)
    
    ch = Channel(17,10e6,BR)
    cfr = CFR_Cartesian_Ideal_FLP_NoLatency(1e-3,1e-3/1000)
    bcheck = BerChecker()
    
    tx_symbs = tx.execute(int(Lsim))
    ch_out = ch.execute(tx_symbs)

    #
    #y, phase_error,integrator,proportional_part,nco_input,nco_output =  cfr_cart.execute(ch_out)
    y, phase_error,integrator,proportional_part,nco_input,nco_output,x_phase, y_phase, y_mod =  cfr_polar.execute(ch_out)

    
    y_cut = y[int(Lsim/2):-1]
    x_cut = tx_symbs[int(Lsim/2):-1]
    

    mse, ser, ber = bcheck.execute(y_cut,x_cut)
    print (10*np.log10(mse))
    print(ser)
    print(ber)
    plots.plot_constellation(y_cut)
    pl.show()

        
    # y_det = slicer_qam16(y)
    # phase_error = -np.angle(y*np.conj(y_det))
    pl.figure()
    plots.plot_cfr_metrics(phase_error, integrator, proportional_part, nco_input, BR)
    pl.show()

    
