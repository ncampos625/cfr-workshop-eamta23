import pylab as pl
import numpy as np

# x is an array of real numbers
def slicer_pam4(x):
    x= np.array(x)
    y=np.zeros([len(x),])
    y[(x<-2)]=-3
    y[(x>-2) & (x<0)]=-1
    y[(x>0) & (x<2)]= 1
    y[(x>2)]= 3
    return y

# x is an array of complex numbers
def slicer_qam16(x):
    return slicer_pam4(np.real(x))+1j*slicer_pam4(np.imag(x))

def slicer_pam4_1sample(x):
    return 3 if (x>2) else (1 if (x>0 and x<2) else (-1 if (x>-2 and x<0) else -3 ))

def slicer_qam16_1sample(x):
    return slicer_pam4_1sample(np.real(x))+1j*slicer_pam4_1sample(np.imag(x))

def slicer_qam16_polar_1sample(xmod, xphase):
    mod1=abs(1+1j)
    mod2=abs(1+1j*3)
    mod3=abs(3+1j*3)
    t1 = 1*0.5*(mod1+mod2)
    t2 = 1*0.5*(mod2+mod3)

    # print(t1)
    # print(t2)

    if xmod<=t1 or xmod>=t2:
        return np.pi/4
    if np.mod(xphase,np.pi/2)>np.pi/4:
        return np.angle(1+3*1j)
    else:
        return np.angle(3+1*1j)
    
    

if __name__=='__main__':
    #print(    slicer_qam16([1.54+1j*0.5,-1.54-1j*.5]))
    #print (slicer_qam16_1sample(2.01+1j*1.25))
    print ( slicer_qam16_polar_1sample(3.1622, 0.1343*np.pi) / np.pi)
