import numpy as np
import pylab as pl
import plots
import qam16_tx as tx

class Channel(object):
    def __init__(self, snr_dB=20, lo_offset=0, BR=32e9):
        """ SNR in dB, LO Offset in Hz, BR in Hz"""
        """ x_power is the incoming signal power in W"""
        self.snr_dB=snr_dB
        self.snr_lin = 10.**(snr_dB/10.)
        self.lo_offset=lo_offset
        self.BR=BR

    def execute(self, x):
        x_power = np.mean(abs(x)**2)
        noise_power = x_power/self.snr_lin
        Lsim=len(x)
        noise_i = np.random.normal(size=(Lsim,))
        noise_q = np.random.normal(size=(Lsim,))
        noise = np.sqrt(noise_power/2)*( noise_i+ 1j*noise_q )
        x_noise = x+noise
        tline =  np.arange(0,Lsim)/self.BR
        lo_rotation = np.exp(1j * 2*np.pi*self.lo_offset *tline )
        return x_noise*lo_rotation

if __name__=='__main__':
    tx=tx.Tx16QAM()
    ch = Channel(20,500e6,32e9)
    
    tx_symbs = tx.execute(int(10e3))
    ch_out = ch.execute(tx_symbs)

    pl.figure()
    pl.subplot(121)
    plots.plot_constellation(tx_symbs, 'Tx out')
    pl.subplot(122)
    plots.plot_constellation(ch_out, 'Ch out')
    pl.show()
