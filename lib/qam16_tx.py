import numpy as np
import pylab as pl
import plots 

class Tx16QAM(object):
    def __init__(self):
        pass
    
    def execute(self, Lsim):
        self.Lsim=Lsim
        dec_symbs_i = np.random.randint(4,size=(Lsim,))
        dec_symbs_q = np.random.randint(4,size=(Lsim,))
        symbs_i = 2*dec_symbs_i-3
        symbs_q = 2*dec_symbs_q-3
        return symbs_i + 1j * symbs_q
        
if __name__== '__main__':
    tx = Tx16QAM()
    tx_symbs = tx.execute(1000)
    plots.plot_constellation(tx_symbs, "Tx Output")
    pl.show()
