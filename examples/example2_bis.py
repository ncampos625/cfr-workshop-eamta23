"""
En este ejemplo mostramos un modelo simplificado del canal de comunicaciones asumiendo:
1. ISI totalmente compensada en el ecualizador
2. Error de clock totalmente compensado
3. El canal solo agrega ruido y offset de portadora

Luego se instancia un Carrier Frequency Recovery (CFR) ideal
4. Se cuenta BER y se mide MSE
5. Se muestran algunas metricas internas del PLL
"""

import sys
sys.path.append('./lib')
import pylab as pl
import numpy as np
from qam16_tx import Tx16QAM
from  channel import Channel
from cfr import CFR_Cartesian_Ideal_FLP_NoLatency
import plots
from ber_checker import BerChecker

# Parametros generales
BR=32e9 # Symbol rate
Lsim=500e3 # Longitud de simulacion
lo_offset=0e6

kps = [5e-2,1e-3]
snrs=[13,15,17,19]
# kps = [5e-2,1e-3]
# snrs=[15,16,17]

# Instanciacion de los modulos
tx=Tx16QAM() 
bcheck = BerChecker()

bers_no_cfr=[]
bers_cfr={}
for ikp, kp in enumerate(kps):
    bers_cfr[kp]=[]
    ki=kp/1000.
    for snr_db in snrs:
        ch = Channel(snr_db,lo_offset,BR)
        cfr_cart = CFR_Cartesian_Ideal_FLP_NoLatency(kp, ki)

        # Ejecucion del modelo
        tx_symbs = tx.execute(int(Lsim))
        ch_out = ch.execute(tx_symbs)

        # Se procesa la salida del canal con el FCR
        cfr_out, phase_error,integrator,proportional_part,nco_input,nco_output =  cfr_cart.execute(ch_out)

        # Recorto las senales para contar BER
        cfr_in_cut = ch_out[int(Lsim/2):-1]
        cfr_out_cut = cfr_out[int(Lsim/2):-1]
        x_cut = tx_symbs[int(Lsim/2):-1]

        # Ber check
        mse_cfr_in, ser, ber_cfr_in = bcheck.execute(cfr_in_cut,x_cut)
        mse_cfr_out, ser, ber_cfr_out = bcheck.execute(cfr_out_cut,x_cut)

        if ikp==0:
            bers_no_cfr.append(ber_cfr_in)
        bers_cfr[kp].append(ber_cfr_out)
        print("")
        print("Resultados para QAM16 @SNR={:2.2f}dB, LO={:2.2f}MHz:".format(snr_db, lo_offset))
        print ("\t -> BER: CFR IN={:.2e} \t - \t CFR OUT={:.2e}".format(ber_cfr_in, ber_cfr_out))


        

pl.figure(figsize=(15,8))
for kp in kps:
    pl.semilogy(snrs, bers_cfr[kp], label='Kp: {:2.2e}'.format(kp), LineWidth=2)
pl.semilogy(snrs, bers_no_cfr, '--k', label='No CFR', LineWidth=2)
pl.grid(1)
pl.legend(loc='best')
pl.xlabel('SNR [dB]')
pl.ylabel('BER')

pl.show()


    
