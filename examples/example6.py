"""
En este ejemplo mostramos un modelo simplificado del canal de comunicaciones asumiendo:
1. ISI totalmente compensada en el ecualizador
2. Error de clock totalmente compensado
3. El canal solo agrega ruido y offset de portadora

Luego se instancia un Carrier Frequency Recovery (CFR) ideal, en paralelo con una instancia del PLL en dominio polar donde se ha cuantizado alguna variable interna del lazo. Se compara el funcionamiento de ambos PLL para distinto numero de bits de cuantizacion
4. Se cuenta BER y se mide MSE
5. Se muestran algunas metricas internas del PLL
"""

import sys
import warnings
warnings.filterwarnings('ignore')
sys.path.append('./lib')
import pylab as pl
import numpy as np
from qam16_tx import Tx16QAM
from  channel import Channel
from cfr import CFR_Cartesian_Ideal_FLP_NoLatency, CFR_Polar_FLP_wLatency
import plots
from ber_checker import BerChecker

# Parametros generales
BR=32e9 # Symbol rate
Lsim=50e3 # Longitud de simulacion
snr_db=17
lo_offset=0.5e6

kp_list = [1e-3,1e-2,5e-2]
latencies = np.arange(0,80,5)

bers = {}

# Instanciacion de los modulos
tx=Tx16QAM() 
ch = Channel(snr_db,lo_offset,BR)
bcheck = BerChecker()


for kp in kp_list:
    bers[kp]=[]
    for latency in latencies:
        ki = kp/1000.

        print("Processing kp={:2.2e}, latency={:d}".format(kp,latency))

        cfr_polar = CFR_Polar_FLP_wLatency(kp,ki,latency)

        # Ejecucion del modelo
        tx_symbs = tx.execute(int(Lsim))
        ch_out = ch.execute(tx_symbs)

        # Se procesa la salida del canal con el FCR
        cfr_out_polar, phase_error_polar,integrator_polar,proportional_part,nco_input,nco_output,x_phase, y_phase, y_mod =  cfr_polar.execute(ch_out)

        # Recorto las senales para contar BER
        cfr_out_polar_cut = cfr_out_polar[int(Lsim/2):-1]
        x_cut = tx_symbs[int(Lsim/2):-1]

        # Ber check
        mse_cfr_out_polar, ser, ber_cfr_out_polar = bcheck.execute(cfr_out_polar_cut,x_cut)
        bers[kp].append(ber_cfr_out_polar)

pl.figure()
for kp in kp_list:
    pl.semilogy(latencies, bers[kp], '-o',label='Kp={:2.2e}'.format(kp))
    
pl.grid(1)
pl.xlabel("Loop Latency")
pl.ylabel("BER")
pl.legend(loc='best')
pl.show()
    

