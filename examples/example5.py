"""
En este ejemplo mostramos un modelo simplificado del canal de comunicaciones asumiendo:
1. ISI totalmente compensada en el ecualizador
2. Error de clock totalmente compensado
3. El canal solo agrega ruido y offset de portadora

Luego se instancia un Carrier Frequency Recovery (CFR) ideal, en paralelo con una instancia del PLL en dominio polar usando CORDIC para computar la fase de entrada. Se compara el funcionamiento de ambos PLL para distinta cantidad de iteraciones del algoritmo CORDIC
4. Se cuenta BER y se mide MSE
5. Se muestran algunas metricas internas del PLL
"""

import sys
import warnings
warnings.filterwarnings('ignore')
sys.path.append('./lib')
import pylab as pl
import numpy as np
from qam16_tx import Tx16QAM
from  channel import Channel
from cfr import CFR_Cartesian_Ideal_FLP_NoLatency, CFR_Polar_SimpSlicer_Cordic_FLP_NoLatency
import plots
from ber_checker import BerChecker

# Parametros generales
BR=32e9 # Symbol rate
Lsim=100e3 # Longitud de simulacion
snr_db=17
lo_offset=10e6

kp = 1e-2
ki = kp/1000.
cordic_iterations = 4

# Instanciacion de los modulos
tx=Tx16QAM() 
ch = Channel(snr_db,lo_offset,BR)
cfr_cart = CFR_Cartesian_Ideal_FLP_NoLatency(kp, ki)
cfr_polar = CFR_Polar_SimpSlicer_Cordic_FLP_NoLatency(kp, ki, cordic_iterations)
bcheck = BerChecker()

# Ejecucion del modelo
tx_symbs = tx.execute(int(Lsim))
ch_out = ch.execute(tx_symbs)

# Se procesa la salida del canal con el FCR
cfr_out_cart, phase_error_cart,integrator_cart,proportional_part,nco_input,nco_output =  cfr_cart.execute(ch_out)
cfr_out_polar, phase_error_polar,integrator_polar,proportional_part,nco_input,nco_output,x_phase, y_phase, y_mod =  cfr_polar.execute(ch_out)

# Recorto las senales para contar BER
cfr_in_cut = ch_out[int(Lsim/2):-1]
cfr_out_cart_cut = cfr_out_cart[int(Lsim/2):-1]
cfr_out_polar_cut = cfr_out_polar[int(Lsim/2):-1]
x_cut = tx_symbs[int(Lsim/2):-1]

# Ber check
mse_cfr_in, ser, ber_cfr_in = bcheck.execute(cfr_in_cut,x_cut)
mse_cfr_out_cart, ser, ber_cfr_out_cart = bcheck.execute(cfr_out_cart_cut,x_cut)
mse_cfr_out_polar, ser, ber_cfr_out_polar = bcheck.execute(cfr_out_polar_cut,x_cut)

print("")
print("Resultados para QAM16 @SNR={:2.2f}dB, LO={:2.2f}MHz, FLP CORDIC with {:d} iterations:".format(snr_db, lo_offset, cordic_iterations))
print ("\t -> MSE: CFR CART={:2.2f}dB \t - \t CFR CORDIC={:2.2f}dB".format(10*np.log10(mse_cfr_out_cart),10*np.log10(mse_cfr_out_polar)))
print ("\t -> BER: CFR CART={:.2e} \t - \t CFR CORDIC={:.2e}".format(ber_cfr_out_cart, ber_cfr_out_polar))


pl.figure(figsize=(15,8))
pl.subplot(131)
plots.plot_constellation(tx_symbs, 'Tx out')
pl.subplot(132)
plots.plot_constellation(ch_out, 'Ch out')
pl.subplot(133)
plots.plot_constellation(cfr_out_polar_cut, 'CFR Out Cordic')

pl.figure(figsize=(15,8))
plots.plot_cfr_metrics(phase_error_cart, integrator_cart, proportional_part, nco_input, BR, label="Ideal")
plots.plot_cfr_metrics(phase_error_polar, integrator_polar, proportional_part, nco_input, BR, label="CORDIC")

pl.show()


    
